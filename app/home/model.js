import DS from 'ember-data';


export default DS.Model.extend({
	username: DS.attr('string'),
	password: DS.attr('string'),
	activeS: DS.attr('boolean', {defaultValue:false}),
}).reopenClass({
	FIXTURES: [
		{
			id: 1,
			username: "pepito",
			password: "pepito",
			activeS: false
		},
		{
			id: 2,
			username: "pepito1",
			password: "pepito1",
			activeS: false
		}

	]
});
